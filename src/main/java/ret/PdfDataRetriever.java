package ret;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import idr.core.extraction.Invoice;
import idr.core.extraction.Retriever;
import idr.core.extraction.DataSeparator;
import idr.core.validation.InvoiceValidator;
import idr.core.validation.Validator;

public class PdfDataRetriever implements Retriever{
	private String[] extensions = {"pdf",".pdf","PDF",".PDF"};
	private Validator validator = new InvoiceValidator();
	
	/**
	 * Convierte el pdf a texto
	 * @param pdfPath
	 * @return
	 * @throws java.io.IOException 
	 */
	private String getText (File pdfFile) throws java.io.IOException{
	    PDDocument doc = PDDocument.load(pdfFile);
	    String result = new PDFTextStripper().getText(doc);
	    return result.trim();
	}

	@Override
	public Invoice retrieveInvoiceData(File file) {
		String data;
		Invoice invoice = new Invoice();
		try {
			data = DataSeparator.extractCuit(getText(file));
			if(validator.isAValidCuit(data)) {
				invoice.setCuitSender(data);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
		return invoice;
	}

	@Override
	public boolean isSuitable(File file) {
		for(String ext: extensions) {
			if(file.getName().endsWith(ext))
				return true;
		}
		return false;
	}
	
}
