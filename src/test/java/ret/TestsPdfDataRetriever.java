package ret;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import idr.core.extraction.Invoice;

public class TestsPdfDataRetriever {
	private PdfDataRetriever ret;
	private final String path = "src/test/resources/InvTest.pdf";
	private File file;
	private Invoice invoice;

	@Before
	public void setUpBeforeClass() throws Exception {
		ret= new PdfDataRetriever();
		file = new File(path);
		invoice = ret.retrieveInvoiceData(file);
	}

	@Test
	public void testIsSuitableForPDF() throws IOException {
		assertTrue(ret.isSuitable(file));
	}
	
	@Test
	public void testReturnsCuit() throws IOException {
		assertEquals("27345932082",invoice.getCuitSender());
	}
	

}
